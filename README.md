# Surrogate cloud fields

## Intro
Surrogate cloud fields share certain, typically statistical, properties with real cloud field: they are surrogates for measured clouds. Using well-known algorithms such as the Fourier method and the Bounded Cascade method the generated fields have a specified Fourier or power spectrum. This page presents a new easy iterative method that allows specifying both the power spectrum and the amplitude distribution of the parameter of interest, like liquid water content or liquid water path. Because of this, the method is ideally suited to generate cloud fields based on measured data and it is able to generate broken cloud fields. The method is based on the Iterative Amplitude Adapted Fourier Transform (IAAFT) algorithm.

## Software
This directory or zip-file contains programs to make surrogate cloud fields using the Iterative 
Amplitude Adapted Fourier Transform (IAAFT) method or the Stochastic Iterative 
Amplitude Adapted Fourier Transform (SIAAFT) method . For more information see:
http://www.meteo.uni-bonn.de/victor/themes/surrogates/iaaft/

Installation
1) Put all files into one (new) directory.
2) Start Matlab.
3) Add the new directory to you path or change into this directory.
4) Run surrogate_1d_1d and see what happens.

### Note
1. The names of all main programs start with surrogate*, the other files are help files.
2. In the m-files surrogate*, there is a function call: load_?d_data(1). By increasing this 
number you can get more examples. See the load_?d_data files to see which numbers make 
sense.
3. Best try the programs in ascending order of dimension: surrogate_1d_1d, surrogate_1d_2d, 
surrogate_2d_2d_horizontal, surrogate_2d_2d_vertical, surrogate_2d_3d, surrogate_3d_3d, or 
in order of increasing complexity: surrogate_1d_1d, surrogate_2d_2d_horizontal, 
surrogate_2d_2d_vertical, surrogate_3d_3d, surrogate_1d_2d, surrogate_2d_3d.

## References
Victor Venema, Steffen Meyer, Sebastián Gimeno García, Anke Kniffka, Clemens Simmer, Susanne Crewell, Ulrich Löhnert, Thomas Trautmann, and Andreas Macke, 2006: Surrogate cloud fields generated with the Iterative Amplitude Adapted Fourier Transform algorithm. Tellus 58A, no 1, pp. 104-120.

Victor Venema, Susanne Bachner, Henning Rust, and Clemens Simmer, 2006: Statistical characteristics of surrogate data based on geophysical measurements
Nonlinear Processes in Geophysics, 13, p. 449-466.

Victor Venema, Felix Ament, Clemens Simmer, 2006: A Stochastic Iterative Amplitude Adapted Fourier Transform Algorithm with improved accuracy
Nonlinear Processes in Geophysics, 13, no. 3, pp. 247-363.
